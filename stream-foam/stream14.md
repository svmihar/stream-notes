# Stream14

1. fastai2 overview, and lesson 2

1. bikin website untuk serving modelnya
	- ~~ktrain paragraph visualizer~~
	- prediksi ini komplain atau ngga
	- EDA? -> streamlit aja apa ya?
	- topic yagn trending

2. label the clustered tweets, that tends to be a complaint
	- [x] lda 

4. refactor kodingan di koinworks biar rapih dan gak saling bergantung

5. setup annoy on koinwork's tweet on flair embedding for the search engine

6. bikin mlops buat tracking modelnnya si koinworks (classifier)

7. bikin text generation buat koinworks

8. tabular data buat betting model  + mlops
	-  ini ganti training engine ke ktrain kayak [disini](https://nbviewer.jupyter.org/github/amaiya/ktrain/blob/develop/tutorials/tutorial-08-tabular_classification_and_regression.ipynb)

9. tutorial gridsome + cms buat blog nya mami

4. graham_613's regression problem on nba competition dataset
	- trying the new mlops tutorial on github actions to review model performance (mae)
	- finding anomalies on the table (by visualizing it of course)
	- matrix factorization using seperated points 
	- make model
		- [x] lightgbm
		- [x] xgboost
		- [ ] tabular data fastai
	- visualization with decision tree viz [here](https://explained.ai/decision-tree-viz/index.html)
	- create frontend (streamlit)
		- give 2 team, what is the points?
		- show match history


5. flask qr reader
	- get the webcam working
	- get the capture maybe like 5 frame per second working 

