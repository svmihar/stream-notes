# Vue

## referensi
[here](https://medium.com/js-dojo/getting-started-with-vuejs-for-web-and-native-285dc64f0f0d)

## output
ngerti props, data(), v-model, methods, dan click events.

## creating new site
```bash 
vue create < nama project>
```
wow hello-world nya aja 200MB, amazing. 
## vue components
liat di buttonCounter di `index.html`
[very good reference to go to](https://vuejs.org/v2/guide/components.html)

### Vue.component
- ini bisa bentuk template

### question on binding
- custom binding pake `$` itu ap aja? terus ngapain ada kalo udah bisa callback dari computed?

### Binding buat ke html
- ini "placeho      lder" ke DOMnya

The `< template>...</template>` tag that contains our component’s HTML
The `< script>...</script>` tag that contains our component’s JavaScript and our component’s Vue instance information (within export default{} )
The optional `<style>...</style>` tags that contain our component-specific CSS'


## binding ke html
### cara bego 
tinggal decalre aja dibawa script, bikin var app, terus bid ke sebuah id di dalam div
```html
<div id="app">
    {{message}}
</div>
```
terus di bawah script tinggal di declare aja nama "app" nya 
### for loop
di html nya paek binding khusus namanya `v-for`: isinya  "todo in todos" yang dimana todos ini adalah json format list, jangan lupa binding html nya di sesuaikan dengan elemen di vue
```html
<div id = 'app-3'>
    <ol>
        <li v-for="todo in todos">
            {{todo.text}}
        </li>
    </ol>
</div>
```

dimana vue nya akan jadi sbb : 
```javascrirpt
var app3 = new Vue({
        el: '#app-3',
        data:{
            todos   : [
                {'text': 'belajar vue'},
                {'text': 'belajar vuex'},
                {'text': 'belajar routing dengan browser'},
                {'text': 'bikin static website'},
                {'text': 'cara load model tensorflow.js ke vue js'}

            ]
        }
    })
```
### bikin component html sendiri dengan Vue Component
pertama delcare dulu vue componentnya naman apa, sama prop apa yang mau di rubah sama vue nya dimana di contoh ini dibuat semacam groceryList yang di tampilin dair data vue nya

```Vue
Vue.component('todo-item', {
props: ['todo'], 
template: '<li>{{todo.text}}</li>'
```

ini mirip banget sama `app3` di index.html, tapi bedanya ini di render dari dalam Vue sebagai sebuah komponen html, bukan html yang di masukin vue
dari situ tinggal dimasukkin aja logic nya : 
```javascript
    var app5 = new Vue({
        el:'#app-5',
        data: {
            groceryList: [
                {id: 0, text:'Coca Cola'},
                {id: 1, text: 'susu'},
                {id: 3, text: 'soju'}
            ]
        },
    })
```
harus diperhatikan bahwa kalo mau bikin gini jangan lupa binding nya di "sematkan" ke dalam props di dalam Vue.componentnya 
v-bind:test='item' -> namanya binding nya harus sama dengan propsnya

## raw html di dalem vue
yes you can render html on vue, on html. lol 
```
<p> {{rawHtml}}
<p>Using v-html directive: <span v-html="rawHtml"></span></p>
```
dimana rawHtml disini adalah sebuah atribute html dengan styling font warna merah, nanti dengan `v-html directive` itu raw html nya bakal di render beneran jadi html bukan raw text nya. 

### warning on rendering html
ini kurang secure karena prone to XSS, jadi selama yang di render itu bukan user input seharusnya sih aman, tapi lebih baik jangan


## computed property
semua property itu jangan di "hitung" di dalam html, sebaiknya dihitung di property computed, [[lifecycle hook]]

terus computed property ini juga bisa manggil fungsi, yang dimana fungsinya harus ngereturn sesuatu

hati hati kalau computed propery ini semacam caching mechanism nya vue, jadi cuman ngereturn ketika dipanggil dan gak terus2an di refresh.

## vue bindings
prefix `v-` itu artinya vue binding dan ada beberapa action yang bisa di pake kayak: 
### v-bind
jadi kalo di html nya ada atribut begini
```html
<div
  class="static"
  v-bind:class="{ active: isActive, 'text-danger': hasError }"
></div>
```
dan punya data di Vue.component nya begini: 
```json
data: {
isActive: true, 
hasError: false}
```
berarti dia akan convert ke DOMnya jadi begin: 
```html
<div class="static active"></div>
```
dan selama variable isActive dan hasError gak berubah ya gak akan berubah juga html yang di DOM
### v-bind array syntax
yang di bind juga bisa pake array, bahkan isi array nya bisa javascript object, terus kalo ada object yang di bind harus pake `{namaObjectHere}`

jadi kayak gini: 
```vue 
< div v-bind:class="[{ active: isActive }, errorClass]"></div>
```

buat naro expression di dalem vue nya
```
v-bind Shorthand
<!-- full syntax -->
<a v-bind:href="url"> ... </a>


<!-- short hand-->
v-bind Shorthand
<!-- full syntax -->
<a v-bind:href="url"> ... </a>

<!-- shorthand -->
<a :href="url"> ... </a>
```	
### v-on
- v-on
buat naro click event atau action ke dalem html (dom)nya
kayak click, atau event handler in general lah intinya

### v-model 
ngembaliin user input (biasanya dari form) ke vue, ini biasanya dari prop sebuah atribut



## questions
- kenapa di created function, `this` itu harus di declare ulang? sama hubungannya dengan computed vs method caching itu gimana?


- gimana cara pake library external kayak axios gitu buat get request atau manggil fungsi di dalem vue component nya?
	- kalo gak pake template, tinggal di declare aja, dan langsung pake
	- kalo pake template berarti harus di import dulu
- App.vue sama main.js ini gunanya apa 
![](https://miro.medium.com/max/700/1*PLqmGOizJB2L5NX9xTHAVA.png)
cek version nya ini pake semver (semantic versioning)
```
(e.g., ./ , @/ , ../ , etc)
```
- apanya sih ini yagn di export ? sama ini kok ada props dengan message itu apaan coba lol 
    ```
    export default {
    name: 'HelloWorld',
   props: {
        msg: String
    }
    }
    ``` 
    yang di declare di export ini jadi attribute html, jadi component html, yang dirender sama Vue, oalah jenius anjir ini yang buat vue
-  kenapa di export modeul di `.vue` ada yang bentuknya function, ada yang bentuknya option-object(kayak struct di go atau dictionary di python?)
- gimana caranya import npm module baru di vue?
	- install dulu via npm/yarn 
		`npx install -g axios
	- kalo pake template tinggal di import (contoh pake axios) `import {{ axios }} from 'axios'`

- ini ada @ di depan attribute component itu artinya apa ya  [ini ada di v-bidnig]
    actionable stuff, jadi bisa manggil method, terus display return methodnya
