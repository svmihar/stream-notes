# Libcudnn

## files needed 
1. cuda_10.1.243_418.87.00_linux.run

## downgrade
1. remove all nvidia shit
```
sudo apt-get remove --purge '^nvidia-.*' && sudo apt-get autoremove && sudo apt-get autoclean && sudo rm -rf /usr/local/cuda*
```

2. add ppa to the repo
```
sudo add-apt-repository ppa:graphics-drivers/ppa
```

3. check the 440 version (buat 20.04)
```
ubuntu-drivers devices
```

4. install the 440 version
```
sudo apt install nvidia-driver-440
```
ini otomatis ngeblock default graphic driver nya (nouveau) dan ngerebuild initrd image nya as stated here 
```
A modprobe blacklist file has been created at /etc/modprobe.d to prevent Nouveau
from loading. This can be reverted by deleting the following file:
/etc/modprobe.d/nvidia-graphics-drivers.conf

A new initrd image has also been created. To revert, please regenerate your
initrd by running the following command after deleting the modprobe.d file:
`/usr/sbin/initramfs -u`

*****************************************************************************
*** Reboot your computer and verify that the NVIDIA graphics driver can   ***
*** be loaded.                                                            ***
****************************************************
```

5. reboot
setelah reboot cek `nvidia-smi` apakah begini keluar apa nggak 
```
Tue Jul 28 14:29:05 2020       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 440.95.01    Driver Version: 440.95.01    CUDA Version: 10.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce 940M        On   | 00000000:04:00.0 Off |                  N/A |
| N/A   68C    P8    N/A /  N/A |      0MiB /  2004MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

```

6. ganti gcc ke versi 8
cuda 10 gak support gcc >8
```
sudo update-alternatives --config gcc
```

7. run the 'run' script
```
sudo bash cuda_10.1.243_418.87.00_linux.run
```

9. configure paths
```
# setup your paths
echo 'export PATH=/usr/local/cuda-10.1/bin:$PATH' >> ~/.zshrc
echo 'export LD_LIBRARY_PATH=/usr/local/cuda-10.1/lib64:$LD_LIBRARY_PATH' >> ~/.zshrc
source ~/.zshrc
sudo ldconfig

```

10. install cudnn
tar -xzvf ${CUDNN_TAR_FILE}


## install pertama kali
