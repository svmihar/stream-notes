# Stream13

0. clustering indo tweets, koinworks complaints mostly based on its flair embeddings (character embeddings, with rnn)
	- [ ] lda 
	- [x] dbscan
	- [x] kmeans

1. setup milvus on koinwork's tweet on flair embeddings

2. graham613's regression problem on nba competition dataset
	- trying the new mlops tutorial on github actions to review model performance (mae)
	- finding anomalies on the table (by visualizing it of course)
	- matrix factorization using seperated points 
	- make model
		- lightgbm
		- xgboost
		- tabulardata fastai
	- visualization with decision tree viz [here](https://explained.ai/decision-tree-viz/index.html)
	- create frontend (streamlit)
		- give 2 team, what is the points?
		- show match history

3.  learn how to package stuff on python (pip install <x>)

4. flask qr reader
	- get the webcam working
	- get the capture maybe like 5 frame per second working 

5. machine learning university
