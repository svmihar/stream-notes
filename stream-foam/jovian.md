# jovian 

## assignment 2
### problem
explain 5 functions from numpy function operations

### solution 
1. np.random -> generate a 3 vectors, with 10 dimension
2. np.svd -> dimensionality reduction
3. np.zeros
4. np.dot
np.diag
5. np.linalg.norm(v1 - v2) -> see what vectors are the closest (smallest angle)
