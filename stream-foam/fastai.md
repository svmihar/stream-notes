# fastai2 

## questionnaire chapter 2
1. similar patterns on the pixels, like cookies and bears
2. generating new patterns by understanding. 
3. humans can't really differentiate which is which.
4. recidivism, alternative to automating a process is human in the loop
5. the data that has less noise. 
6. black box, you can't really explain the "embeddings"
7. ?
8. stats on steroids: drivetrain approach 
	- you tune it till the accuracy get good
9. github.com/svmihar/yet-another-image-classification
	- xxx.herokuapp.com/predict/<base64>
10.  Batches of item you got from a datablock to be trained on
11. four things you need to create a dataloader
	- X, category (image, tensor, text)
	- Y, (target to predict)
	- splitter, how do you split the training / where do you split the training
	- the actual path of the file
12. it specifies how to split a certain type of data
13. so you give a random_seed, where it'll reproduce the same random result at first 
14. X, and y or X_train, y_train, X_test, y_Test (train_Test_split)
15. differencess within: 
	- resize: stretching image to a certain scale
	- squish: making a reflection? to the image so that it can conform to a certain scale 
	- pad: adding black bars to "crop" the image
	- crop: literal crop
16. to prevent overfitting, and get the model to have a better understanding of the dataset
17. `item_tfms` is a sequentially transformed data, and the `batch_Tfms`is a grouped transformed data to be trained on a gpu 
18. it's to distinguish the type1 and type2 error: TN, TP, FN, FP
19. A pickled model (python object), which contains the arch.
20. inference
21. GUI part of jupyter notebook
22. CPU -> small scale, GPU -> larger scale
23. better environemnt: stable and controlable  (easier to debug when wrong)
24. type1, type2 error
25. data that aren't trained (yet) within the model
26. when a larger part of the data got changed into another 
27. gather -> train -> serve
28. AGI -> SGI, then tech singularity. lol

