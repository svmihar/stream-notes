# image clustering


dataset shopee
pake model nya resnet50

- [ ] similar image api with efficientnet tf [here](https://www.kaggle.com/cdeotte/rapids-cuml-knn-find-duplicates) on the shopee dataset
	- [x] download dataset
	- [x] get all images (glob) -> list of image path
	- [ ] cluster sesuai dengan berapa kelasnya dia
		- efnet has this "swish" conv stemmed layer.  dari sini 
```python 
    def extract_features(self, inputs):
        """use convolution layer to extract feature .
        Args:
            inputs (tensor): Input tensor.
        Returns:
            Output of the final convolution 
            layer in the efficientnet model.
        """
        # Stem
        x = self._swish(self._bn0(self._conv_stem(inputs)))

        # Blocks
        for idx, block in enumerate(self._blocks):
            drop_connect_rate = self._global_params.drop_connect_rate
            if drop_connect_rate:
                drop_connect_rate *= float(idx) / len(self._blocks) # scale drop connect_rate
            x = block(x, drop_connect_rate=drop_connect_rate)
        
        # Head
        x = self._swish(self._bn1(self._conv_head(x)))

        return x```
```


		- jadi pretrained efnet di tensorflow, punya parameter `no_top` yagn gak punya _fc layer. ternyata udah ada api nya langsung wkwk tapi harus pake pretrained model nya dari mereka
		- [x] convert ipynb to py
		- [ ] saving the feature vectors with `np.save`
	- [ ] annoy image search?

- image nya di convert ke vektor pake `resnet50` baseline
    - pake class `SaveHook` nya pytorch

## todo
- [ ] bikin knn pake cuml
- [ ] bikin tsne pake cuTSNE + rapids ai
- [ ] bikin streamlit frontend
- [ ] bikin local ngrok, khusus buat display image nya 
