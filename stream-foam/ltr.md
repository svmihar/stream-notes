# Learning to Rank - LGBM


## metrics used
- ndcg
	normalized discounted normalized gain
	given a rank, tebak rank nya, yang salah dibagi dengan log loss
	[sklearn implementation](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.ndcg_score.html)
- err 
	literally tebak ranking given a document on a set of ranked documents
dua metric di atas cuman cek pairwise, sementara yang sebenernya matters itu kenapa urutannya begitu, here we go lambdarank

## Ranknet
- supervised, must have ranking list first, to be trained on
## LambdaRank
- lambda rank nyari gradient kenapa suatu urutan itu bisa naik dan turun, terus dicek dari "turunan" pertama + ndcg
- computed after the sort is done

## MART
- regression tree, wth is a regression tree?

## LightGBM
### dataset format
- train
- test
- valid
ini harus specify `group`, yang dimana apanya yang di group? rankingnya juga gak gitu jelas di examples rada beda dengan python implementation

buat make ranker harus ada group of result, dari hasil query yang tepat, (ya iyalah, ndcg kan butuh true positive nya)

kita gak punya data ini 
```python 
grouparray-like
		Group/query data, used for ranking task.
```

### ganti ltr ke classic regression problem
- given a user, and an item, tebak berapa ratingnya dia?

## parameter tuning
pake optuna

### baseline
- baseline done di 	`lightgbm regressor baseline.csv`

### grid search
```python 
Best params: {'verbose': 2, 'boosting_type': 'gbdt', 'objective': 'regression', 'metric': 'rmse', 'lambda_l1': 1.2460807058525907e-07, 'lambda_l2': 6.85770962169098, 'num_leaves': 31, 'feature_fraction': 0.7, 'bagging_fraction': 1.0, 'bagging_freq': 0, 'min_child_samples': 5}
``` pake optuna



For heavily unbalanced datasets such as 1:10000:

- max_bin: keep it only for memory pressure, not to tune (otherwise overfitting)
- learning rate: keep it only for training speed, not to tune (otherwise overfitting)
- n_estimators: must be infinite (like 9999999) and use early stopping to auto-tune (otherwise overfitting)
- num_leaves: [7, 4095]
- max_depth: [2, 63] and infinite (I personally saw metric performance increases with such 63 depth with small number of leaves on sparse unbalanced datasets)
- scale_pos_weight: [1, 10000] (if over 10000, something might be wrong because I never saw it that good after 5000)
- min_child_weight: [0.01, (sample size / 1000)] if you are using logloss (think about the hessian possible value range before putting "sample size / 1000", it is dataset-dependent and loss-dependent)
- subsample: [0.4, 1]
- bagging_freq: only 1, keep as is (otherwise overfitting)
- colsample_bytree: [0.4, 1]
- is_unbalance: false (make your own weighting with scale_pos_weight)
- USE A CUSTOM METRIC (to reflect reality without weighting, otherwise you have weights inside your metric with premade metrics like xgboost)

Never tune these parameters unless you have an explicit requirement to tune them:

Learning rate (lower means longer to train but more accurate, higher means smaller to train but less accurate)
Number of boosting iterations (automatically tuned with early stopping and learning rate)
Maximum number of bins (RAM dependent)

