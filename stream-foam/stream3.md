# Stream3

### image clustering
- ~~get feature vectors dari efnet pretrained~~ dataset 40GB nyangkut, jadi lama buat "nyalain" servernya
- ~~train the baseline fastai image classification (this'll be long)~~ lagi training paling selesai jam 14-15
- ~~get the image vectors using fastai hooks~~
- ~~annoy image search~~
- get similar image using cuml knn
- bikin tsne pake cuTSNE + rapids ai
- streamlit frontend?  dunno about this, bahkan gak tau apakah bakal muat apa kagak di heroku? bisa sih tapi versi annoy nya doang, dan harus bikin index buat upload semua gambarnya lol

---

### recommender
- trying new graphlab's recommendation framework
	- user - user (clustering based on user features)
	- ~~user - item (clustering based on user history)~~~ got .9 MSE on fastai's collaborative filter model
	- apriori (cart sequence prediction)

### search-query
- ~~perbaiki encoding pake flair~~ ternyata ada yang salah return
- ~~build rest api on flask~~ tunggu PR review
	- ~~tfidf~~
	- ~~fasttext + listing_w2v~~

- new encoding method
	- ~~hyperhyper + sentence pooling (average, median, max)~~ dep. library nya deprecated, udah pindah jadi plotline 
- reserach new encoding method
	- fastai's ulmfit
	- han sentence encoder


# POST STREAM
STREAM DONE! see you next stream :)   
[part 1](https://youtu.be/gsJMgVkxfAs)     
[part 2](https://youtu.be/BRU5Q78AnBs)     


