# Stream12

0. pretrain flair embeddings on koinworks' tweet
	- will achieve better embedding
	- see if similar tweets are actually similar

1. checkout vimfiler

2. make search engine for koinworks tweet

3. graham613's regression problem on nba competition dataset
	- understand the problem
	- make model
		- lightgbm
		- xgboost
	- create frontend (streamlit)

4.  learn how to package stuff on python (pip install <x>)
