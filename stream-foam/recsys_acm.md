# interesting papers to read
- [bert on conversation recommendation](https://arxiv.org/pdf/2007.15356)
- RecSeats: A Hybrid Convolutional Neural Network Choice Model for Seat Recommendations at Reserved Seating Venues the  papers isn't published with doi yet
- FISSA: Fusing Item Similarity Models with Self-Attention Networks for Sequential Recommendation paper not yet published
