# Stream2

## stream 2

- [ ] exploring HAN (hierarchical attention network) -> sentence encoding
	- [x] get embedding vectors
	- [x] apply to the search-query
	- [x] rubah pooling jadi max, cek dengan median
	- [ ] test case buat load / dump encoder di ft + w2v listing
			- itu ada error di if flow, padhala gak ada yang ngeencode ngapain dia nyala 
	- [ ] bikin flask rest api buat nyobain masing2 method

- [ ] coba hyperhyper buat gantiin tfidf biasa
	- [ ] get the embedding vector???

- [ ] indo-news-dataset with ulmfit
	- [ ] ulmfit finetuning langsung (pake punya om cahya) dari labeled kompas
	- [ ] ulmfit pretraining with indo-news-dataset
	- [ ] onnx? fastinference?
	- [ ] streamlit frontend  / fastapi / flask rest

- [ ] rich brian's newest video to pantoptic segmentation
	- [ ] only label people

- [ ] belajar pytorch lagi
	- [ ] darimana?
	- [ ] https://github.com/yoseflaw/KArgo
- [ ] belajar ONNX
	- [ ] dari [abishek](https://www.youtube.com/watch?v=7nutT3Aacyw)
	- [ ] apply ke bert indo-news-dataset
		- [ ] ini masih perlu finetuning lagi  -> simpletransformers