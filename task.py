#! /home/svmihar/miniconda/bin/python

from fire import Fire
from typing import List
import string
import datetime
from dataclasses import dataclass
from subprocess import call
import pandas as pd

from rich.console import Console
from rich.markdown import Markdown

console = Console()
punctuations = list(string.punctuation)
TEST_MODE = False


@dataclass
class Task:
    name: str
    status: str = "NOT SET"  # CURRENT, DONE
    date_created: datetime = datetime.datetime.now()
    date_done: datetime = None

    def done(self):
        self.status = True
        self.date_done = datetime.datetime.now()

    def set_status(self, stat):
        if stat.lower() not in ["current", "done", "not set"]:
            console.print("WRONG", "STATS", style="bold red")
            raise ValueError
        self.status = stat.upper()

    def to_dict(self):
        temp = {
            "name": self.name,
            "status": self.status,
            "date_created": self.date_created.strftime("%m/%d/%Y, %H:%M:%S"),
        }
        if self.date_done is not None:
            temp["date_done"] = (self.date_done.strftime("%m/%d/%Y, %H:%M:%S"),)
        return temp


def main(md):
    from pathlib import Path

    # check md exist
    md_file = Path(md)
    if not md_file.is_file():
        raise FileNotFoundError
    all_tasks, task_title = md_to_df(md)
    while True:
        task_handler = handle(all_tasks, 12)
        task_handler.handle()

    # rewrite task.txt to the next task
    # repeat until task == 0


@dataclass
class handle:
    tasks: List
    task_title: str

    def change_current(self, i):
        current_index = self.get_current(return_index=True)
        if current_index is not None:
            self.done(current_index)

        console.print(
            f"changing current task to {self.tasks[i].name}", style="bold blue"
        )
        self.tasks[i].set_status("current")
        set_task(self.get_current().name[2:])

    def done(self, i, md_path=None):
        console.print(f"congrats", style="bold green")
        self.tasks[i].set_status("done")
        # TODO: write to md file

    def get_current(self, return_index=False):
        for i, task in enumerate(self.tasks):
            if task.status.lower() == "current":
                if return_index:
                    return i
                else:
                    return self.tasks[i]
        return None

    def get_current_txt(self, test=TEST_MODE):
        if test:
            filename = "_current_task.txt"
        else:
            filename = "current_task.txt"
        return open(filename).readlines()[0]

    def set_custom_task(self, task_name):
        current_index = self.get_current(return_index=True)
        if current_index:
            self.tasks[current_index].set_status("NOT SET")
        set_task(task_name)

    def get_tasks(self):
        return md_formatter(self.tasks, self.task_title)

    def print(self):
        console.clear()
        md_tasks = self.get_tasks()
        md = Markdown(md_tasks)
        console.print(md)

    def handle(self):
        self.print()
        task_status = [t.status for t in self.tasks]
        console.print(
            f"current task in txt is: {self.get_current_txt()}", style="bold green"
        )

        if "current" not in [t.status.lower() for t in self.tasks]:
            console.print("no current is set", style="italic green")
            i = (
                int(
                    console.input(
                        "insert [b] a number [/b]current todo to be displayed\n>"
                    )
                )
                - 1
            )
            self.change_current(i)
        self.print()

        console.print(
            f"current task in txt is: {self.get_current_txt()}", style="bold green"
        )
        console.print("1.change current\n2.set task to done\n3. set custom task")
        n = console.input("task_choice?\n>")
        n = int(n)
        if n == 1:
            choice = console.input("which one?")
            self.change_current(int(choice) - 1)
        elif n == 2:
            choice = console.input("which one?")
            self.done(int(choice) - 1)
        elif n == 3:
            task_name = console.input("task name?\n>")
            self.set_custom_task(task_name)
        else:
            console.print("error bego", style="bold red")
            console.input("enter to continue")

        self.print()


def set_task(task_name: str, test=TEST_MODE):
    filename = "current_task.txt"
    if test:
        filename = "_current_task.txt"
    with open(filename, "w") as f:
        f.writelines(task_name + " | ")


def remove_non_alphanum(s: str):
    for punc in punctuations:
        s = s.replace(punc, "")
    return s.strip()


def md_formatter(list_of_tasks, stream_title):
    tasks = []
    for i, task in enumerate(list_of_tasks):
        tasks.append(f"{i+1}. {task.name[2:]} - {task.status}")
        tasks.append("\n")
    task_md = "\n".join(tasks)
    md = f"# {stream_title} \n{task_md}"
    return md


def md_to_df(md: str) -> pd.DataFrame:
    """
    name: <task_name>
    status: <[done, null]>
    """
    tasks = open(md).read().splitlines()
    title = tasks[0].replace("#", "")
    tasks = [a for a in tasks if a]  # bersihin none
    tasks = [
        remove_non_alphanum(a) for a in tasks if a[0].isdigit()
    ]  # bersihin sub-task
    tasks = [Task(a) for a in tasks]
    # return {t.to_dict() for t in tasks}
    return tasks, title


def df_to_md(original_md: List[str], df: pd.DataFrame) -> str:
    pass


if __name__ == "__main__":
    Fire()
