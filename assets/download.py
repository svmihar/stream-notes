#!/home/svmihar/miniconda/bin/python

import os
from fire import Fire
from tqdm import tqdm

MUSIC_FOLDER = "/home/svmihar/secondary-hdd/NAS/streaming-music/music"  # dipindah soalnya kegedean kalo simpen di ssd


def youtube(link):
    os.system(f'youtube-dl -f m4a {link} --output "{MUSIC_FOLDER}/%(title)s.%(ext)s"')
    convert()


def convert():
    os.system("conda deactivate")
    music_files = os.listdir(MUSIC_FOLDER)
    m4a = [f"{MUSIC_FOLDER}/{a}" for a in music_files if a.endswith("m4a")]
    if not m4a:
        print("no m4a detected in music folder")
        return

    def remove_m4a_format(f):
        return f.split(".m4a")[0]

    for m in tqdm(m4a):
        hasil = remove_m4a_format(m) + ".mp3"
        os.system(f'ffmpeg -i "{m}" "{hasil}" -y')
        print(f"deleting {m}")
        os.system(f'rm "{m}"')


if __name__ == "__main__":
    Fire()
